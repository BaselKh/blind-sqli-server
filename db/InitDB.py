from app import db
from flask import current_app


db.create_all()


def init_db():
    with current_app.open_resource('BaseSchemas.sql') as f:
        db.executescript(f.read().decode('utf8'))


# @click.command('init-db')
# @with_appcontext
# def init_db_command():
#     """Clear the existing data and create new tables."""
#     init_db()
#     click.echo('Initialized the database.')
