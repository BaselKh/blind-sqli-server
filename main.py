from app import app
from db.InitDB import init_db

if __name__ == '__main__':
    app.run()
    init_db()
